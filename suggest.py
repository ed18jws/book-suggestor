from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.options import Options as FirefoxOptions
import sys, time
import re as regex
import argparse

DELIM=','


options=FirefoxOptions()
options.add_argument("--headless")
driver = webdriver.Firefox(options=options)
driver.get("https://www.goodreads.com") 

class Book:
    def __init__(self, title):
        self.title = title
        self.author = ''
        self.release_year = 0
        self.genres = []
        self.similar_titles = []
        self.get_attributes()
    def get_attributes(self):
        driver.get("https://www.goodreads.com")
        searchbox = driver.find_element_by_xpath("//input[@name='query']")
        searchbox.send_keys(self.title)
        searchbox.send_keys(Keys.RETURN)
        time.sleep(3)
        try:
            driver.find_element_by_xpath('/html/body/div[3]/div/div/div[1]/button').click()
        except NoSuchElementException:
            pass
        try:
            driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[2]/table/tbody/tr[1]/td[2]/a').click()            
            time.sleep(3)
            self.author = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[4]/div[1]/div[2]/div[1]/span[2]/div[1]/a/span').text
            try: 
                temp = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[4]/div[1]/div[2]/div[5]/div[2]/nobr').text
            except NoSuchElementException:
                temp = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[4]/div[1]/div[2]/div[5]/div[2]').text            
            year = regex.findall('(\d{4})', temp)
            self.release_year = year[0]
            x= 0
            try: 
                temp = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[5]/div[7]/div/div[2]/div/div[1]/div[1]/a').text
                try:
                    temp_sub = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[5]/div[7]/div/div[2]/div/div[1]/div[1]/a[2]').text
                    self.genres.append(temp_sub)
                except NoSuchElementException:
                    self.genres.append(temp)
            except NoSuchElementException:
                pass
            try:
                temp = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[5]/div[7]/div/div[2]/div/div[2]/div[1]/a').text
                try:
                    temp_sub = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[5]/div[7]/div/div[2]/div/div[2]/div[1]/a[2]').text
                    self.genres.append(temp_sub)
                except NoSuchElementException:
                    self.genres.append(temp)
            except NoSuchElementException:
                pass
            try:
                temp = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[5]/div[7]/div/div[2]/div/div[3]/div[1]/a').text
                try:
                    temp_sub = driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[5]/div[7]/div/div[2]/div/div[3]/div[1]/a[2]').text
                    self.genres.append(temp_sub)
                except NoSuchElementException:
                    self.genres.append(temp)
            except NoSuchElementException:
                pass 
            try:
                driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/div[2]/div[5]/div[3]/div/div[2]/div/a').click()
                try:
                    title = driver.find_element_by_xpath('/html/body/div[4]/div/div[3]/div[1]/div[3]/div/div/div[1]/div/div/div[2]/a/span').text
                    self.similar_titles.append(title)
                except NoSuchElementException:
                    pass
                try:
                    title = driver.find_element_by_xpath('/html/body/div[4]/div/div[3]/div[1]/div[3]/div/div/div[2]/div/div/div[2]/a/span').text
                    self.similar_titles.append(title)
                except NoSuchElementException:
                    pass
                try:
                    title = driver.find_element_by_xpath('/html/body/div[4]/div/div[3]/div[1]/div[5]/div/div[1]/div/div/div[2]/a/span').text
                    self.similar_titles.append(title)
                except NoSuchElementException:
                    pass
            except NoSuchElementException:
                pass

        except NoSuchElementException:
            return False
        return self.author, self.release_year, self.genres
    
    def __str__(self):
        return 'Title: {}\nAuthor: {}\nRelease Year: {}\nGenres: {}\nSimilar Titles: {}\n'.format(self.title, self.author, self.release_year, self.genres, self.similar_titles)


class BookSuggestions:
    def __init__(self, args):
        self.books = self.load_books(args)
        self.suggestions = self.get_suggestions()
        self.genres = self.get_genres()
        self.date_range = self.get_date_range()
        print (self.books, self.genres, self.date_range)

    def load_books(self, args):
        books =[]
        for arg in args:
            book = Book(arg)
            books.append(book)
        return books

    def get_genres(self):
        genres = []
        for book in self.books:
            for genre in book.genres:
                if not genre in genres:
                    genres.append(genre)
        return genres
        
    def get_date_range(self):
        dates = []
        for book in self.books:
            if not book.release_year in dates:
                dates.append(book.release_year)
        date_lower = min(dates)
        date_upper = max(dates)
        date_range = {'min': date_lower, 'max': date_upper}
        return date_range

    def get_suggestions(self):
        suggestions = []
        titles = []
        x=0
        for book in self.books:
            titles.append(book.title)
            for similar_title in book.similar_titles:
                if not similar_title in titles:
                    titles.append(similar_title)
                    similar_book = Book(similar_title)
                    if not similar_book in self.books:
                        suggestions.append(similar_book)
                        print (str(similar_book))
                        x+=1
                        print(x)
        """
        for book in suggestions:
            for similar_title in book.similar_titles:
                if not similar_title in titles:
                    similar_book = Book(similar_title)
                    if not similar_book in self.books:
                        suggestions.append(similar_book)
                        print (str(similar_book))
                        x+=1
                        print(x)
        """
        return suggestions

    def filter_suggestions(self):
        return

    def __str__(self):
        string =''
        for book in self.suggestions:
            string+=str(book)

def main(argv):
    if argv is None:
        books = 'random'
    else:
        books = argv
    bs = BookSuggestions(books)
    print (str(bs))
    #book = Book("Paper Towns")
    #time.sleep(5)
    #print(book.get_attributes())

if __name__ == '__main__':
    #    args = []
    #if len(sys.argv) > 1:
    #    for arg in sys.argv:
    #        args.append(arg)
    #    main(args)
    #else:
    #    main(None)
    books = ['Crime and Punishment']
    main(books)
